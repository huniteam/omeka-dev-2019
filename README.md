# HuNI Development Stack

This repository creates a unified docker-compose stack that runs both the HuNI web app and the HuNI harvest service.

## Install

Clone this repo with the `--recursive` flag to also clone the submodules.

`git clone --recursive https://bitbucket.org/huniteam/omeka-dev-2019.git`

## Docker Compose

Start the docker compose stack with `docker-compose up`

Open a browser to http://localhost:2000/

The stack has been configured to run on http://localhost:2000/ - if you need remote access, you'll need to set up an ssh tunnel.

eg. `ssh -f $devhost -L 2000:localhost:2000 -N`

## Development feed

There is a built-in development feed server available at http://localhost:2000/dev-feed/resources.xml

The resources.xml file is generated dynamically, so any files placed in dev-feed/files will be immediately available as the latest feed.

This feed is internally configured as "Omeka2".

## Scripts

There are a number of convenience scripts in scripts/ - these typically call into docker containers. These should be called from the top-level project directory.

These all operate on the Omeka2 feed. They typically get executed in the order they are listed below.

`./scripts/fetch`
  * Fetch the latest harvest. Specify -d to delete the existing harvest and re-fetch.

`./scripts/preprocess`
  * Preprocess the harvest. Validate and classify the new records.

`./scripts/solr-upload`
  * Transform the preprocessed records and upload to solr.

The following environment variables can be used with the scripts above:
  * HUNI_DEBUG - turn on extra debug output
  * HUNI_QUIET - don't show the per-minute status updates on long jobs
  * DBIC_TRACE - show SQL queries

If the solr transforms are modified, this script will sync those changes back to the database:

`./scripts/update-transforms`

There's also a script which will remove all Omeka2 documents - useful if you want to blow everything away and harvest again from scratch.

`./scriptw/clean-all`
