use 5.28.1;
use warnings;

use Mojolicious::Lite;
use Crypt::Digest::MD5 qw( md5_file_hex );
use Path::Tiny qw( path );

my $root = $ENV{HUNI_DEV_FEED_ROOT}
    or die '$HUNI_DEV_FEED_ROOT not set';

push @{ app->static->paths } => $ENV{HUNI_DEV_FEED_ROOT};

get '/resources.xml' => sub {
    my ($c) = @_;
    my $root = path($ENV{HUNI_DEV_FEED_ROOT});
    my @children = sort $root->children( qr/\.xml$/ );
    my $records = @children;

    $c->res->headers->content_type('application/xml');

    my $prefix = qq(<?xml version="1.0" encoding="utf-8"?>\n<resources recordCount="$records">\n);
    my $suffix = qq(</resources>\n);

    my $drain;
    my $i = 0;
    $drain = sub {
        my ($c) = @_;
        if ($i < $records) {
            my $file = $children[$i++];
            my $md5 = md5_file_hex($file->stringify);
            my $basename = $file->basename;
            my $chunk = qq(  <record><name>$basename</name><hash>$md5</hash></record>\n);
            $c->write_chunk($chunk, $drain);
        }
        else {
            $c->write_chunk($suffix, sub { $_[0]->finish } );
        }
    };

    $c->write_chunk($prefix, $drain);
};

app->start;
