#!/usr/bin/env perl

use 5.22.0;
use warnings;
use Path::Tiny qw( path );

my $filename;
my @lines;
while (my $line = <STDIN>) {
    next if $line =~ /<\/?datacrate/;

    push(@lines, $line);

    if ($line =~ /<meta name="\@id".*>(.*)<\/meta>/) {
        $filename = $1;
        $filename =~ s!^.*://!!;
        $filename =~ s!/!_!g;
        $filename .= '.xml';
    }
    elsif ($line =~ /<\/record>/) {
        path($filename)->spew_utf8(@lines);
        @lines = ();
        $filename = undef;
    }
}
